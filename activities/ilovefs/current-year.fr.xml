<?xml version="1.0" encoding="UTF-8"?>

<data>
  <version>2</version>
  <module>

    <h2>Journée J'♥ les Logiciels Libres en 2022</h2>

    <p>
      La journée J'aime les Logiciels Libres de cette année se concentre sur les jeux Libres. Les jeux, et en particulier les jeux Libres, ne sont pas seulement une activité amusante : ils sont une part importante de nos vies quotidiennes. En jouant, nous pouvons rencontrer de nouveaux ou de vieux amis, nous détendre, acquérir de nouvelles compétences, et le plus important : nous pouvons contribuer à notre jeu Libre favori et l'adapter à nos propres souhaits. Par conséquent, nous organisons un évènement spécial dédié aux jeux vidéo Libres. Y participeront trois experts en jeux Libres, moteurs de jeux et Game Jams.</p>
      <p>Le <a href="https://download.fsfe.org/campaigns/ilovefs/games/Games%20Event/IloveFS22%20Agenda.pdf">agenda de l'événement</a> ressemble à ceci:
      <ul>
      <li>18:00 - 18:05 (CET): Bienvenue et introduction</li>
      <li>18:05 - 18:25 (CET): "Flare - Free/Libre Action Roleplaying Engine" par Justin Jacobs</li>
      <li>18:25 - 18:45 (CET): "Vassal – Free Software Game Engine" par Joel Uckelman</li>
      <li>18:45 - 19:05 (CET): "Godot Wild Jams" par Kati Baker</li>
      <li>19:05 - 19:20 (CET): "Veloren –Free Software Game" par Forest Anderson</li>
      <li>19:20 - 20:00 (CET): Temps de jeu</li>
      <li>20:00 (CET): Adieu</li>
      </ul>
      Pour plus d'informations sur l'évènement, consultez notre <a href="/news/2022/news-20220201-01.html">article</a>.
    </p>

    <p>
      Nous disons également <strong>merci</strong> à vous tous, qui avez contribué à des projets de Logiciels Libres. Utilisons la journée J'aime les Logiciels Libres pour dire merci et célébrer notre amour pour les Logiciels Libres.
    </p>

    <figure>
      <img
        src="https://pics.fsfe.org/uploads/medium/69395a3acc8cfb98fbb058d0b44a8f04.png"
        alt="Gaming event picture" />
          <figcaption>Nous célébrons la journée #ilovefs !</figcaption>
    </figure>

    <p>
      Pour la journée J'aime les Logiciels Libres de cette année, nous avons un nouvel épisode de podcast sur la Liberté des Logiciels. Notre invité pour cet épisode spécial est Stanislas Dolcini ; ensemble, Bonnie et Stanislas parlent du jeu Libre 0 A.D.: Empires Ascendant. L'épisode sera bientôt livré.
    </p>


    <h2>Prenez part à la journée J'aime les Logiciels Libres</h2>

    <div class="icon-grid">
      <ul>
        <li>
          <img src="/graphics/icons/ilovefs-use.png" alt="ILoveFS heart with 'use'"/>
          <div>
            <h3>Utilisez l'opportunité</h3>
            <p>
              Saisissez l'opportunité de ce jour merveilleux pour remercier les personnes qui vous permettent de profiter de la liberté des logiciels. Utilisez nos nouveaux <a href="https://download.fsfe.org/campaigns/ilovefs/games/">J'aime les Logiciels Libres : Graphismes de l'évènement des jeux</a> et notre épisode de podcast sur la Liberté des Logiciels à venir.
              <a href="https://download.fsfe.org/campaigns/ilovefs/share-pics/">Images à partager</a>,
              <a href="/contribute/spreadtheword#ilovefs">autocollants et ballons</a>,
              <a href="/activities/ilovefs/artwork/artwork.html">illustrations</a>, et
              <a href="/order/order.html">marchandises</a> fournis par la FSFE pour <em>#ilovefs</em>.
            </p>

          </div>
        </li>

        <li>
          <img src="/graphics/icons/ilovefs-study.png" alt="ILoveFS heart with 'study'"/>
          <div>
            <h3>Pensez-y</h3>
            <p>
              Pensez à : Quel projet de Logiciel Libre avez-vous utilisé durant la dernière année ? À quel jeu Libre avez-vous joué avec vos amis ou seul ? Comment vous-êtes vous amusé en utilisant ou parce que vous avez utilisé un Logiciel Libre ? Pour obtenir de l'inspiration, jetez un œil aux <a href="/activities/ilovefs/whylovefs/gallery.html">déclarations d'amour</a> des autres.
            </p>
          </div>
        </li>

        <li>
          <img src="/graphics/icons/ilovefs-share.png" alt="ILoveFS heart with 'share'"/>
          <div>
            <h3>Partagez votre amour</h3>
            <p>
              Ceci est la partie amusante ! Nous avons créé un <a href="https://sharepic.fsfe.org/">générateur d'images à partager</a>, avec lequel vous pouvez facilement créer vos propres images à partager et partager votre appréciation sur les médias sociaux (<em>#ilovefs</em>), dans des billets de blog, des messages imagés ou vidéos, ou directement aux développeurs et contributeurs des Logiciels Libres.
            </p>
          </div>
        </li>

        <li>
          <img src="/graphics/icons/ilovefs-improve.png" alt="ILoveFS heart with 'improve'"/>
          <div>
            <h3>Améliorer les Logiciels Libres</h3>
            <p>
              La discussion est d'argent, la contribution est d'or ! Aidez un projet de Logiciel Libre avec du code, une traduction, ou en assistant ses utilisateurs. Ou si vous pouvez, envisagez de donner à une organisation de Logiciels Libres <a href="https://my.fsfe.org/donate">comme la FSFE</a>, ou à un autre projet.
            </p>
          </div>
        </li>
      </ul>
    </div>

    <figure class="no-border">
      <img
        src="https://pics.fsfe.org/uploads/small/f164c8517e78449fc8844a72347af490.png"
        alt="#ilovefs" />
    </figure>

    <p>
      Quelle est votre contribution à cette journée spéciale et quelles sont les personnes qui vous permettent d'utiliser des Logiciels Libres ? Allez-vous utiliser nos autocollants et ballons ? Ou faire une image ou une vidéo avec notre nouvelle chemise ILoveFS ? Et que diriez-vous de célébrer la liberté des logiciels avec vos collègues et amis dans un rassemblement d'entreprise ou un évènement public ? Quoi que vous fassiez, montrez-le au monde en utilisant le mot-dièse <em>#ilovefs</em>. Et si vous avez des questions, <a href="/contact/">envoyez-nous simplement un courriel</a>.
    </p>

    <p>
      Joyeuse journée <strong><span class="text-danger">J'aime les Logiciels Libres</span></strong> à tout le monde ! ❤
    </p>

  </module>

  <translator>Sylvain Chiron</translator>
</data>
